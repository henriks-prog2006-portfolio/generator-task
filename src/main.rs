//Generator task
use std::collections::HashMap;
use std::io;
use std::path::Path;
use rand::{distributions::Alphanumeric, Rng};

fn main() -> io::Result<()> {
    // Prompting the user and reading the number of IDs to create
    println!("Please input number of IDs to create:");
    let len = read_num();
    // Prompting the user and reading the maximum allowed occurrences per ID
    println!("Please input maximum allowed occurrences per ID:");
    let max_repeats = read_num();
    // Prompting the user and reading the length of each ID
    println!("Please input the length of each ID:");
    let id_length = read_num();

    // Generating IDs and storing in a HashMap and Vec
    let (h_map, vector) = create_ids(len, max_repeats, id_length);
    // Specifying the file path to save IDs
    let file_path = Path::new("./data.txt");
    // Writing the generated IDs to the file
    std::fs::write(file_path, vector.join("\n"))?;

    // Checking for the maximum occurrences of any ID
    let repeats = check_occurrences(&h_map);
    // Printing the result of the max repeats check
    println!("max repeats: {:?}", repeats.unwrap_or_else(|| String::from("No IDs found")));

    Ok(())
}

// Reads a line from standard input, expects a valid usize number, and returns it.
// Panics with an error message if input is not a valid number.
fn read_num() -> usize {
    // Initializing a string to store user input
    let mut input = String::new();
    // Reading a line from standard input
    io::stdin().read_line(&mut input).expect("Failed to read line");
    // Parsing the input string to a number and returning
    input.trim().parse().expect("Please enter a number")
}

// Generates a specified number of unique IDs with a set maximum repeats and specific length.
// Returns a HashMap for occurrences and a Vector of IDs.
fn create_ids(num_ids: usize, max_repeats: usize, id_length: usize) -> (HashMap<String, usize>, Vec<String>) {
    // Creating a new HashMap to store IDs and their occurrences
    let mut h_map = HashMap::new();
    // Creating a vector to store the IDs
    let mut vector = Vec::new();

    // Generating IDs until the desired number is reached
    while h_map.len() < num_ids {
        // Generating a random alphanumeric ID of specified length
        let id: String = rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(id_length)
            .map(char::from)
            .collect();

        // Inserting or updating the count of each ID in the HashMap
        let count = h_map.entry(id.clone()).or_insert(0);
        // Ensuring no ID exceeds the maximum repeat limit
        if *count < max_repeats {
            // Adding the ID to the vector
            vector.push(id);
            // Incrementing the count of this ID
            *count += 1;
        }
    }
    // Returning both the HashMap and vector containing the IDs
    (h_map, vector)
}

// Returns the highest occurrence of any ID from the provided
// HashMap as a String, or None if empty.
fn check_occurrences(h_map: &HashMap<String, usize>) -> Option<String> {
    // Finding the maximum value in the HashMap which represents the highest occurrence of any ID
    let max_repeats = h_map.values().max().unwrap_or(&0);
    // Converting the maximum number of repeats to a String and returning
    Some(max_repeats.to_string())
}


#[cfg(test)]
mod tests {
    use super::*;

    // Tests `create_ids` to ensure it generates exactly 100 unique IDs with no repeats.
    #[test]
    fn test_create_ids_unique() {
        let (h_map, vector) = create_ids(100, 1, 10);
        assert_eq!(vector.len(), 100); // Checks if 100 IDs were generated
        assert_eq!(h_map.len(), 100);  // Checks if all IDs are unique
        assert!(h_map.values().all(|&v| v == 1)); // Verifies each ID only occurs once
    }

    // Verifies that `create_ids` respects the maximum repeat limit per ID.
    #[test]
    fn test_create_ids_repeats() {
        let (h_map, _) = create_ids(200, 5, 10);
        assert!(h_map.values().all(|&v| v <= 5)); // Ensures no ID is repeated more than 5 times
    }

    // Tests `check_occurrences` to confirm it correctly identifies the highest occurrence count.
    #[test]
    fn test_check_occurrences() {
        let mut h_map = HashMap::new();
        h_map.insert("abc123".to_string(), 3); // Sample ID with 3 occurrences
        h_map.insert("def456".to_string(), 5); // Sample ID with the highest 5 occurrences
        h_map.insert("ghi789".to_string(), 2); // Sample ID with 2 occurrences

        let result = check_occurrences(&h_map);
        assert_eq!(result, Some("5".to_string())); // Checks if the highest occurrence is correctly reported as 5
    }
}