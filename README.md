# Random Generator (Rust)

----------------
### Prelim. info

*Crates used for this program:*

- rand = "0.8.5"


# The task: Generate random IDs

The task is to create a set amount of random IDs according to the guidelines
> - User specifies length
> - User specifies amount of entries
> - User specifies max amount of repeated entries
> - stores the output in a file

#### Example Use:
```
Please input number of IDs to create:
1000000
Please input maximum allowed occurrences per ID:
4
Please input the length of each ID:
5
max repeats: "2"

```

--------------
### Functions implemented:

#### read_num() -> usize
Reads a line from standard input, expects a valid usize number, and returns it.
#### create_ids(num_ids: usize, max_repeats: usize, id_length: usize) -> (HashMap<String, usize>, Vec<String>)
Generates a specified number of unique IDs with a set maximum repeats and specific length
#### check_occurrences(h_map: &HashMap<String, usize>) -> Option<String
Returns the highest occurrence of any ID from the provided

